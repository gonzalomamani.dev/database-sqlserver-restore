#!/bin/bash

DIRECTORY_PATH=`readlink -f $(dirname $0)`
source "${DIRECTORY_PATH}/lib/library.sh"
source "${DIRECTORY_PATH}/config/config.sh"
echo  ${DIRECTORY_PATH}
for i in $(ls ${DIRECTORY_PATH_BACK}); do
	name_array=$(split $i "_")
	fecha_array=$(search_date "$(echo ${name_array[@]})")
	if [ -n "${fecha_array}" ]; then
		DIR=$(date_to_directory "$(echo ${fecha_array[@]})")
		if [ -n ${DIR} ]; then
			cd $DIRECTORY_PATH_BACK
			tmp=$(mktemp -d)
			echo "1. Comprimiendo ..."
			tar -czvf "${i}.tar.gz" "${i}"
			echo "2. Enviando a ${SERVER_DB}${SERVER_PATH}${DIR} ..."
			sftp ${USER_DB}@${SERVER_DB}:${SERVER_PATH}${DIR} <<< $'put '${DIRECTORY_PATH}'/'${i}'.tar.gz'
			cd $DIRECTORY_PATH
		fi
	fi
done
rm -R ${DIRECTORY_PATH_LAST}/*
mv ${DIRECTORY_PATH_BACK}/*.bak ${DIRECTORY_PATH_LAST}/
rm -R ${DIRECTORY_PATH_BACK}/*


















