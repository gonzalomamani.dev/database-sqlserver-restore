#!/bin/bash

function split () {
	echo $(echo "$1" | tr "$2" "\n")
	return 0;
}

function search_date () {
	result_array=();
	for element in ${1}; do
		result_array+=($element)
	done
	if [ ${#result_array[@]} -gt 1 ]; then
		fecha=$(echo ${result_array[${#result_array[@]}-2]} | tr "-" "\n")
		echo ${fecha}
	else
		echo "";
	fi
	return 0;
}
function date_to_directory() {
	result_array=();
	for element in ${1}; do
		result_array+=($element)
	done
	if [ ${#result_array[@]} -eq 3 ]; then
		DIR=${result_array[0]}"/"${result_array[1]}"/"${result_array[2]}
		echo ${DIR}
	else
		echo ""
	fi
	return 0;
}